﻿using System;
using System.Collections.Generic;

namespace Strategy
{
    // Интерфейс Стратегии объявляет операции, общие для всех поддерживаемых
    // версий некоторого алгоритма.
    public interface IPaymentStrategy
    {
        void Pay(int amount);
    }

    // Конкретные Стратегии реализуют алгоритм, следуя базовому интерфейсу
    // Стратегии.
    public class CreditCardStrategy : IPaymentStrategy
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string CardVerificationValue { get; set; }
        public DateTime ExpiryDate { get; set; }
        
        public void Pay(int amount)
        {
            Console.WriteLine($"{amount} paid with credit/debit card");
        }
    }

    public class PaypalStrategy : IPaymentStrategy
    {
        public string Email { get; set; }
        public string Password { get; set; }
        
        public void Pay(int amount)
        {
            Console.WriteLine($"{amount} paid using Paypal.");
        }
    }

    public class Item
    {
        public string Code { get; set; }
        public int Price { get; set; }
    }
    
    // Корзина выступает в роли Контекста
    public class ShoppingCart
    {
        private List<Item> _items = new List<Item>();
        
        public void Add(Item item)
        {
            _items.Add(item);
        }

        public void Remove(Item item)
        {
            _items.Remove(item);
        }

        public int GetTotal()
        {
            int total = 0;

            foreach (var item in _items)
            {
                total += item.Price;
            }

            return total;
        }

        // Вместо того, чтобы самостоятельно реализовывать множественные версии
        // алгоритма, Контекст делегирует некоторую работу объекту Стратегии.
        //
        // Контекст не знает конкретного класса стратегии.
        // Он должен работать со всеми стратегиями через интерфейс Стратегии.
        public void Pay(IPaymentStrategy payment)
        {
            payment.Pay(GetTotal());
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var cart = new ShoppingCart();

            var firstItem = new Item
            {
                Code = "1234",
                Price = 1000
            };

            var secondItem = new Item
            {
                Code = "5678",
                Price = 4500
            };

            cart.Add(firstItem);
            cart.Add(secondItem);

            // paypal оплата 
            cart.Pay (new PaypalStrategy
            {
                Email = "myemail@example.com",
                Password = "mypassword"
            });

            // оплата кредитной(банковской) картой
            cart.Pay (new CreditCardStrategy
            {
                Name = "MyName",
                Code = "1234567890123456",
                CardVerificationValue = "786",
                ExpiryDate = new DateTime(2019, 11, 09)
            });

            Console.ReadLine();
        }
    }
}
