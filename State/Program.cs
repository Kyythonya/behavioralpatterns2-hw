﻿using System;

namespace State
{
    // Контекстом выступает здесь Светильник
    // Хранит ссылку на экземпляр подкласса Состояния, который отображает
    // текущее состояние Контекста.
    public class Light
    {
        // Ссылка на текущее состояние Светильника.
        private State _state = null;

        public Light(State state)
        {
            SetState(state);
        }

        // Позволяет изменять объект Состояния во время выполнения.
        public void SetState(State state)
        {
            _state = state;
            _state.Set(this);
        }

        // Делегирует часть своего поведения текущему объекту Состояния.
        public void Turn()
        {
            _state.Handle();
        }
    }

    // Базовый класс Состояния объявляет методы, которые должны реализовать все
    // Конкретные Состояния, а также предоставляет обратную ссылку на объект
    // Контекст, связанный с Состоянием. Эта обратная ссылка может
    // использоваться Состояниями для передачи Контекста другому Состоянию.
    public abstract class State
    {
        protected Light _light;

        public void Set(Light light)
        {
            _light = light;
        }

        public abstract void Handle();
    }

    // Конкретные Состояния реализуют различные модели поведения, связанные с
    // состоянием Контекста.
    class TurnedOnState : State
    {
        public override void Handle()
        {
            Console.WriteLine("Lights turned on");
            _light.SetState(new TurnedOffState());
        }
    }

    class TurnedOffState : State
    {
        public override void Handle()
        {
            Console.WriteLine("Lights turned off");
            _light.SetState(new TurnedOnState());
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var context = new Light(new TurnedOffState());
            context.Turn();
            context.Turn();
            context.Turn();

            Console.ReadLine();
        }
    }
}
